package com.example.dto;

import com.sun.istack.internal.NotNull;

public class StudentTeacherDTO {

    @NotNull
    private long studentId;

    @NotNull
    private long teacherId;
}
