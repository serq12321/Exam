package com.example.client;

import com.example.dto.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;


@Component
public class ActiveMQClient {

    private final Logger LOGGER = LoggerFactory.getLogger(ActiveMQClient.class);

    @JmsListener(destination = "log-queue")
    public void messageListener(Message message) {
        LOGGER.info("Message {} received", message);
    }
}
