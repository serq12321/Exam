package com.example.client;

import com.example.grpc.LogServiceGrpc;
import com.example.grpc.LogServiceOuterClass;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class GRPCClient {

    private final Logger LOGGER = LoggerFactory.getLogger(GRPCClient.class);

    public void sendMessage(String message) {
        ManagedChannel channel = ManagedChannelBuilder
                .forTarget("localhost:8091")
                .usePlaintext()
                .build();

        LogServiceGrpc.LogServiceBlockingStub stub =
                LogServiceGrpc.newBlockingStub(channel);

        LogServiceOuterClass.LogRequest request = LogServiceOuterClass.LogRequest
                .newBuilder().setOperation(message).build();

        LOGGER.info("request: " + request);

        LogServiceOuterClass.LogResponse response = stub.logging(request);

        LOGGER.info("response: " + response);

        channel.shutdownNow();
    }
}
