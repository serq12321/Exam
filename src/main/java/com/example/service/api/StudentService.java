package com.example.service.api;

import com.example.dto.StudentDTO;
import com.example.dto.TeacherDTO;

import java.util.List;

public interface StudentService {

    void save(StudentDTO studentDTO);

    StudentDTO getById(int id);

    List<StudentDTO> getAll();

    void update(StudentDTO studentDTO);

    void deleteById(int id);

    void addTeacher(int studentId, int teacherId);

    void deleteTeacher(int studentId, int teacherId);

    List<TeacherDTO> getAllTeachersByStudentId(int id);
}
