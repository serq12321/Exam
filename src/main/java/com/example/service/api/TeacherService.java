package com.example.service.api;

import com.example.dto.StudentDTO;
import com.example.dto.TeacherDTO;

import java.util.List;

public interface TeacherService {
    void save(TeacherDTO teacherDTO);

    TeacherDTO getById(int id);

    List<TeacherDTO> getAll();

    void update(TeacherDTO teacherDTO);

    void deleteById(int id);

    void addStudent(int studentId, int teacherId);

    void deleteStudent(int studentId, int teacherId);

    List<StudentDTO> getAllStudentsByTeacherId(int id);
}
