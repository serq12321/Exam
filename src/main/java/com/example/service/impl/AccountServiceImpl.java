package com.example.service.impl;

import com.example.dao.AccountMapper;
import com.example.dto.AccountDTO;
import com.example.model.AccountEntity;
import com.example.service.api.AccountService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountServiceImpl implements AccountService {

    ModelMapper modelMapper;
    AccountMapper accountMapper;

    @Autowired
    public AccountServiceImpl(ModelMapper modelMapper, AccountMapper accountRepository) {
        this.modelMapper = modelMapper;
        this.accountMapper = accountRepository;
    }

    @Override
    public void save(AccountDTO accountDTO) {
        accountMapper.save(modelMapper.map(accountDTO, AccountEntity.class));
    }

    @Override
    public AccountDTO getByUsername(String username) {
        return modelMapper.map(accountMapper.getByUsername(username), AccountDTO.class);
    }

    @Override
    public List<AccountDTO> getAll() {
        return accountMapper.getAll().stream()
                .map(accountEntity -> modelMapper.map(accountEntity, AccountDTO.class))
                .collect(Collectors.toList());
    }
}

