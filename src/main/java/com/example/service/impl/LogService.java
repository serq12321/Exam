package com.example.service.impl;

import com.example.grpc.LogServiceOuterClass;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

import java.io.IOException;


public class LogService extends com.example.grpc.LogServiceGrpc.LogServiceImplBase{
    public static void main(String[] args) {
        Server server = ServerBuilder
                .forPort(8091)
                .addService(new LogService())
                .build();
        startServer(server);
        awaitTerminationServer(server);
    }

    private static void startServer(Server server) {
        try {
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static void awaitTerminationServer(Server server) {
        try {
            server.awaitTermination();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void logging(LogServiceOuterClass.LogRequest request, StreamObserver<LogServiceOuterClass.LogResponse> responseObserver) {
        LogServiceOuterClass.LogResponse response = LogServiceOuterClass.LogResponse
                .newBuilder().setLog(request.getOperation())
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
