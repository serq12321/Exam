package com.example.service.impl;

import com.example.dao.TeacherMapper;
import com.example.dto.StudentDTO;
import com.example.dto.TeacherDTO;
import com.example.model.StudentTeacherEntity;
import com.example.model.TeacherEntity;
import com.example.service.api.TeacherService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeacherServiceImpl implements TeacherService {
    ModelMapper modelMapper;
    TeacherMapper teacherMapper;

    @Autowired
    public TeacherServiceImpl(ModelMapper modelMapper, TeacherMapper teacherMapper) {
        this.modelMapper = modelMapper;
        this.teacherMapper = teacherMapper;
    }

    @Override
    public void save(TeacherDTO teacherDTO) {
        teacherMapper.save(modelMapper.map(teacherDTO, TeacherEntity.class));
    }

    @Override
    public TeacherDTO getById(int id) {
        return modelMapper.map(teacherMapper.getById(id), TeacherDTO.class);
    }

    @Override
    public List<TeacherDTO> getAll() {
        return teacherMapper.getAll().stream()
                .map(teacherEntity -> modelMapper.map(teacherEntity, TeacherDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public void update(TeacherDTO teacherDTO) {
        teacherMapper.update(modelMapper.map(teacherDTO, TeacherEntity.class));
    }

    @Override
    public void deleteById(int id) {
        teacherMapper.deleteById(id);
    }

    @Override
    public void addStudent(int studentId, int teacherId) {
        teacherMapper.addStudent(new StudentTeacherEntity(studentId, teacherId));
    }

    @Override
    public void deleteStudent(int studentId, int teacherId) {
        teacherMapper.deleteStudent(new StudentTeacherEntity(studentId, teacherId));
    }

    @Override
    public List<StudentDTO> getAllStudentsByTeacherId(int id) {
        return teacherMapper.getAllStudentsByTeacherId(id).stream()
                .map(studentEntity -> modelMapper.map(studentEntity, StudentDTO.class))
                .collect(Collectors.toList());
    }
}
