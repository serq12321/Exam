package com.example.service.impl;

import com.example.client.GRPCClient;
import com.example.dao.StudentMapper;
import com.example.dto.StudentDTO;
import com.example.dto.TeacherDTO;
import com.example.model.StudentEntity;
import com.example.model.StudentTeacherEntity;
import com.example.service.api.StudentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements StudentService {

    ModelMapper modelMapper;
    StudentMapper studentMapper;
    GRPCClient logClientGRPC;

    @Autowired
    public StudentServiceImpl(ModelMapper modelMapper, StudentMapper studentMapper, GRPCClient logClientGRPC) {
        this.modelMapper = modelMapper;
        this.studentMapper = studentMapper;
        this.logClientGRPC = logClientGRPC;
    }

    @Override
    public void save(StudentDTO studentDTO) {
        studentMapper.save(modelMapper.map(studentDTO, StudentEntity.class));
        logClientGRPC.sendMessage(studentDTO + " was saved");
    }

    @Override
    public StudentDTO getById(int id) {
        StudentEntity studentEntity = studentMapper.getById(id);
        logClientGRPC.sendMessage(studentEntity + " was shown");
        return modelMapper.map(studentEntity, StudentDTO.class);
    }

    @Override
    public List<StudentDTO> getAll() {
        logClientGRPC.sendMessage("the list of all students was shown");
        return studentMapper.getAll().stream()
                .map(studentEntity -> modelMapper.map(studentEntity, StudentDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public void update(StudentDTO studentDTO) {
        studentMapper.update(modelMapper.map(studentDTO, StudentEntity.class));
        logClientGRPC.sendMessage("student with id = " + studentDTO.getStudentId() + " was update to " + studentDTO);
    }

    @Override
    public void deleteById(int id) {
        studentMapper.deleteById(id);
        logClientGRPC.sendMessage("student with id = " + id + " was deleted");
    }

    @Override
    public void addTeacher(int studentId, int teacherId) {
        studentMapper.addTeacher(new StudentTeacherEntity(studentId, teacherId));
        logClientGRPC.sendMessage("teacher wit id = " + teacherId + " was added to student with id = " + studentId);
    }

    @Override
    public void deleteTeacher(int studentId, int teacherId) {
        studentMapper.deleteTeacher(new StudentTeacherEntity(studentId, teacherId) );
        logClientGRPC.sendMessage("teacher wit id = " + teacherId + " was deleted from student with id = " + studentId);
    }

    @Override
    public List<TeacherDTO> getAllTeachersByStudentId(int id) {
        logClientGRPC.sendMessage("the list of teachers of student with id = " + id + " was shown");
        return studentMapper.getAllTeachersByStudentId(id).stream()
                .map(teacherEntity -> modelMapper.map(teacherEntity, TeacherDTO.class))
                .collect(Collectors.toList());
    }
}
