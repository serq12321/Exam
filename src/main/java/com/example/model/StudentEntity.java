package com.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentEntity {

    private long studentId;

    private String firstName;

    private String lastName;

    private String middleName;

    private String speciality;

    private int course;
}
