package com.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TeacherEntity {

    private long teacherId;

    private String firstName;

    private String lastName;

    private String middleName;

    private String department;
}
