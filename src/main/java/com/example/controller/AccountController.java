package com.example.controller;

import com.example.dto.AccountDTO;
import com.example.dto.Message;
import com.example.service.impl.AccountServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.util.List;

@RestController
@RequestMapping("accounts")
public class AccountController {

    private AccountServiceImpl accountService;
    private JmsTemplate jmsTemplate;

    @Autowired
    public AccountController(AccountServiceImpl accountService, JmsTemplate jmsTemplate) {
        this.accountService = accountService;
        this.jmsTemplate = jmsTemplate;
    }

    @PostMapping("save")
    public void save(@RequestBody @Validated AccountDTO accountDTO) {


        accountService.save(accountDTO);

        Message message = new Message("save person " + accountDTO.toString());

        jmsTemplate.convertAndSend("log-queue", message);
    }

    @GetMapping("{username}")
    public AccountDTO getByUsername(@PathVariable String username) {
        Message message = new Message("get person with username = " + username);

        jmsTemplate.convertAndSend("log-queue", message);

        return accountService.getByUsername(username);
    }

    @GetMapping("getAll")
    public List<AccountDTO> getAll() {
        Message message = new Message("get all people");

        jmsTemplate.convertAndSend("log-queue", message);

        return accountService.getAll();
    }
}
