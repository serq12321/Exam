package com.example.controller;

import com.example.dto.StudentDTO;
import com.example.dto.TeacherDTO;
import com.example.service.api.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("students")
public class StudentController {

    StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping("save")
    public void save(@RequestBody @Validated StudentDTO studentDTO) {
        studentService.save(studentDTO);
    }

    @GetMapping("{id}")
    public StudentDTO getById(@PathVariable int id) {
        return studentService.getById(id);
    }

    @GetMapping("getAll")
    public List<StudentDTO> getAll() {
        return studentService.getAll();
    }

    @PutMapping("{id}/update")
    public void update(@RequestBody @Validated StudentDTO studentDTO, @PathVariable int id) {
        studentDTO.setStudentId(id);
        studentService.update(studentDTO);
    }

    @DeleteMapping("{id}/delete")
    public void delete(@PathVariable int id) {
        studentService.deleteById(id);
    }

    @GetMapping("{id}/getAllTeachers")
    public List<TeacherDTO> getAllTeachersByStudentId(@PathVariable int id) {
        return studentService.getAllTeachersByStudentId(id);
    }

    @PostMapping("{studentId}/addTeacher/{id}")
    public void addTeacher(@PathVariable int studentId, @PathVariable int id) {
        studentService.addTeacher(studentId, id);
    }

    @DeleteMapping("{studentId}/deleteTeacher/{id}")
    public void deleteTeacher(@PathVariable int studentId, @PathVariable int id) {
        studentService.deleteTeacher(studentId, id);
    }


}
