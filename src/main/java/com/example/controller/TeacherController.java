package com.example.controller;

import com.example.dto.StudentDTO;
import com.example.dto.TeacherDTO;
import com.example.service.api.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("teachers")
public class TeacherController {
    TeacherService teacherService;

    @Autowired
    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @PostMapping("save")
    public void save(@RequestBody @Validated TeacherDTO teachersDTO) {
        teacherService.save(teachersDTO);
    }

    @GetMapping("{id}")
    public TeacherDTO getById(@PathVariable int id) {
        return teacherService.getById(id);
    }

    @GetMapping("getAll")
    public List<TeacherDTO> getAll() {
        return teacherService.getAll();
    }

    @PutMapping("{id}/update")
    public void update(@RequestBody @Validated TeacherDTO teachersDTO, @PathVariable int id) {
        teachersDTO.setTeacherId(id);
        teacherService.update(teachersDTO);
    }

    @DeleteMapping("{id}/delete")
    public void delete(@PathVariable int id) {
        teacherService.deleteById(id);
    }

    @GetMapping("{id}/getAllStudents")
    public List<StudentDTO> getAllStudentsByTeacherId(@PathVariable int id) {
        return teacherService.getAllStudentsByTeacherId(id);
    }

    @PostMapping("{teacherId}/addStudent/{id}")
    public void addStudent(@PathVariable int id, @PathVariable int teacherId) {
        teacherService.addStudent(id, teacherId);
    }

    @DeleteMapping("{teacherId}/deleteStudent/{id}")
    public void deleteTeacher(@PathVariable int id, @PathVariable int teacherId) {
        teacherService.deleteStudent(id, teacherId);
    }
}
