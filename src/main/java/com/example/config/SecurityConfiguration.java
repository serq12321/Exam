package com.example.config;

import com.example.security.MyUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    MyUserDetailService myUserDetailService;
    DataSource dataSource;

    @Autowired
    public SecurityConfiguration(MyUserDetailService myUserDetailService, DataSource dataSource) {
        this.myUserDetailService = myUserDetailService;
        this.dataSource = dataSource;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(myUserDetailService)
                .passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/accounts/**").hasRole("ADMIN")
                .antMatchers("/**/save").hasRole("ADMIN")
                .antMatchers("/**/update").hasRole("ADMIN")
                .antMatchers("/**/delete").hasRole("ADMIN")
                .antMatchers("/**/deleteTeacher/**").hasRole("ADMIN")
                .antMatchers("/**/addTeacher/**").hasRole("ADMIN")
                .antMatchers("/").hasAnyRole("ADMIN", "USER")
                .and().httpBasic();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }
}
