package com.example.dao;

import com.example.model.StudentEntity;
import com.example.model.StudentTeacherEntity;
import com.example.model.TeacherEntity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TeacherMapper {

    @Insert("INSERT INTO teachers (first_name, last_name, middle_name, department) " +
            "VALUES (#{firstName}, #{lastName}, #{middleName}, #{department})")
    void save(TeacherEntity teacherEntity);

    @Select("SELECT * FROM teachers WHERE teacher_id = #{teachersId}")
    TeacherEntity getById(int id);

    @Select("SELECT * FROM teachers")
    List<TeacherEntity> getAll();

    @Update("UPDATE teachers " +
            "SET first_name = #{firstName}, " +
            "last_name = #{lastName}, " +
            "middle_name = #{middleName}, " +
            "department = #{department} " +
            "WHERE teacher_id = #{teacherId}")
    void update(TeacherEntity teacherEntity);

    @Delete("DELETE FROM teachers WHERE teacher_id = #{teacherId}")
    void deleteById(int id);

    @Insert("INSERT INTO students_teachers (student_id, teacher_id) " +
            "VALUES (#{studentId}, #{teacherId})")
    void addStudent(StudentTeacherEntity studentTeacherEntity);

    @Delete("DELETE FROM students_teachers " +
            "WHERE student_id = #{studentId} AND " +
            "teacher_id = #{teacherId}")
    void deleteStudent(StudentTeacherEntity studentTeacherEntity);

    @Select("SELECT * FROM students AS s " +
            "JOIN students_teachers AS st " +
            "ON s.student_id = st.student_id " +
            "WHERE teacher_id = #{id}")
    List<StudentEntity> getAllStudentsByTeacherId(int id);
}
