package com.example.dao;

import com.example.model.StudentEntity;
import com.example.model.StudentTeacherEntity;
import com.example.model.TeacherEntity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface StudentMapper {

    @Insert("INSERT INTO students (first_name, last_name, middle_name, speciality, course) " +
            "VALUES (#{firstName}, #{lastName}, #{middleName}, #{speciality}, #{course})")
    void save(StudentEntity studentEntity);

    @Select("SELECT * FROM students WHERE student_id = #{studentId}")
    StudentEntity getById(int id);

    @Select("SELECT * FROM students")
    List<StudentEntity> getAll();

    @Update("UPDATE students " +
            "SET first_name = #{firstName}, " +
            "last_name = #{lastName}, " +
            "middle_name = #{middleName}, " +
            "speciality = #{speciality}, " +
            "course = #{course} " +
            "WHERE student_id = #{studentId}")
    void update(StudentEntity studentEntity);

    @Delete("DELETE FROM students WHERE student_id = #{studentId}")
    void deleteById(int id);

    @Insert("INSERT INTO students_teachers (student_id, teacher_id) " +
            "VALUES (#{studentId}, #{teacherId})")
    void addTeacher(StudentTeacherEntity studentTeacherEntity);

    @Delete("DELETE FROM students_teachers " +
            "WHERE student_id = #{studentId} AND " +
            "teacher_id = #{teacherId}")
    void deleteTeacher(StudentTeacherEntity studentTeacherEntity);

    @Select("SELECT * FROM teachers AS t " +
            "JOIN students_teachers AS st " +
            "ON t.teacher_id = st.teacher_id " +
            "WHERE student_id = #{id}")
    List<TeacherEntity> getAllTeachersByStudentId(int id);
}
