DROP TABLE IF EXISTS students;
DROP SEQUENCE IF EXISTS students_student_id_seq;

DROP TABLE IF EXISTS teachers;
DROP SEQUENCE IF EXISTS teacthers_teacher_id_seq;

DROP TABLE IF EXISTS students_teachers;

DROP TABLE IF EXISTS accounts;
DROP SEQUENCE IF EXISTS accounts_account_id_seq;