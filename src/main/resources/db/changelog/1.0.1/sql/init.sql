CREATE TABLE students
(
    student_id SERIAL NOT NULL,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    middle_name VARCHAR(50) NOT NULL,
    speciality VARCHAR(100) NOT NULL,
    course INT NOT NULL,
    PRIMARY KEY (student_id)
);


CREATE TABLE teachers
(
    teacher_id SERIAL NOT NULL,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    middle_name VARCHAR(50) NOT NULL,
    department VARCHAR(100) NOT NULL,
    PRIMARY KEY (teacher_id)
);

CREATE TABLE students_teachers
(
    student_id BIGINT NOT NULL,
    teacher_id BIGINT NOT NULL,
    PRIMARY KEY (student_id, teacher_id),
    FOREIGN KEY (student_id) REFERENCES students(student_id),
    FOREIGN KEY (teacher_id) REFERENCES teachers(teacher_id)
);

CREATE TABLE accounts
(
    account_id SERIAL,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    role VARCHAR NOT NULL,
    active BOOLEAN NOT NULL,
    PRIMARY KEY (account_id),
    UNIQUE (username)
);

INSERT INTO accounts (username, password, role, active) VALUES ('admin', 'admin', 'ROLE_ADMIN', true);
